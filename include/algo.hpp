// Algorithms

#ifndef ALGO_HPP
#define ALGO_HPP

#include <functional>

#include "Model.hpp"

namespace ai
{

ModelPtr train(ModelPtr mdl, std::function<double(ModelPtr)> count_score, int epoch_count, double max_rate, double rate_change, int batch_size);

double activator_normal(double x);

double activator_sigmoid(double x);

}

#endif
