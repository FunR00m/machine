#ifndef MODEL_HPP
#define MODEL_HPP

#include <vector>
#include <memory>
#include <functional>

namespace ai
{

class Model
{
public:
    Model(std::vector<int> layers, std::function<double(double)> activator_func);
    Model(std::shared_ptr<Model> model);

    void change(double rate);

    std::vector<double> process(std::vector<double> input);

    int get_layers_count();

    int get_layer_size(int layer);

    std::vector< std::vector<double> > neurons;
    std::vector< std::vector<double> > weights;
    std::vector< std::vector<double> > biases;
    std::function<double(double)> activator;

};

using ModelPtr = std::shared_ptr<ai::Model>;

}

#endif