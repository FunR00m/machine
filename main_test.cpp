// First test

#include <iostream>
#include <functional>

#include "Model.hpp"

using ModelPtr = std::shared_ptr<ai::Model>;

ModelPtr train(ModelPtr mdl, std::function<double(ModelPtr)> count_score)
{
    ModelPtr best = mdl;

    double rate = 0.1;

    for(int i = 0; i < 10000; i++)
    {
        double best_score = 0;
        ModelPtr current_best = nullptr;

        for(int j = 0; j < 100; j++)
        {
            ModelPtr current = std::make_shared<ai::Model>(best);
            current->change(rate);
            double score = count_score(current);

            if(score > best_score || current_best == nullptr)
            {
                best_score = score;
                current_best = current;
            }
        }

        std::cout << "EPOCH " << i << " Best score: " << best_score << std::endl;
        best = current_best;
        rate *= 0.999;
    }

    return best;
}

double test_1(ModelPtr model)
{
    double score = 0;
    for(int z = 0; z < 10; z++)
    {
        double task = (double)rand() / RAND_MAX * 100 - 50;
        double out = model->process({ task })[0];
        score += -abs(task * 5 - out);
    }

    return score;
}

double activator_normal(double x)
{
    return x;
}

int main()
{
    srand(122);

    std::vector layers = { 1, 10, 10, 1 };
    ModelPtr model = train(std::make_shared<ai::Model>(layers, activator_normal), test_1);

    bool shell = true;
    std::cout << "===SHELL===\n";
    while(shell)
    {
        std::string command;
        std::cout << "> ";
        std::cin >> command;
        if(command == "exit")
        {
            shell = false;
            break;
        }

        double task = std::stoi(command);
        double out = model->process({ task })[0];
        std::cout << out << std::endl;
    }
}
