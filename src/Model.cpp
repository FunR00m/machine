#include "Model.hpp"

#include <cstdlib>
#include <iostream>

namespace ai
{

Model::Model(std::shared_ptr<Model> model)
{
    neurons     = model->neurons;
    biases      = model->biases;
    weights     = model->weights;
    activator   = model->activator;
}

Model::Model(std::vector<int> layers, std::function<double(double)> activator_func)
{
    activator = activator_func;

    int prev_count = -1;

    for(int neurons_count : layers)
    {
        neurons.push_back(std::vector<double>(neurons_count, 0));
        biases.push_back(std::vector<double>(neurons_count, 0));

        for(int i = 0; i < neurons_count; i++)
        {
            biases.back()[i] = ((double)rand()) / RAND_MAX;
        }

        if(prev_count != -1)
        {
            weights.push_back(std::vector<double>(neurons_count * prev_count));
            for(int i = 0; i < neurons_count; i++)
            {
                for(int j = 0; j < prev_count; j++)
                {
                    weights.back()[i * prev_count + j] = ((double)rand()) / RAND_MAX;
                }
            }
        }

        prev_count = neurons_count;
    }
}

std::vector<double> Model::process(std::vector<double> input)
{
    assert(get_layer_size(0) == input.size() && "Input layers size does not match.");
    neurons[0] = input;

    for(int layer = 0; layer < get_layers_count() - 1; layer++)
    {
        // std::cout << "get_layers_count " << get_layers_count() << std::endl;
        // std::cout << "get_layer_size(" << layer + 1 << ") " << get_layer_size(layer + 1) << std::endl;
        for(int neuron = 0; neuron < get_layer_size(layer + 1); neuron++)
        {
            neurons[layer + 1][neuron] = biases[layer + 1][neuron];
            for(int current = 0; current < get_layer_size(layer); current++)
            {
                neurons[layer + 1][neuron] += neurons[layer][current] *
                            weights[layer][get_layer_size(layer) * neuron + current];
            }
            neurons[layer + 1][neuron] = activator(neurons[layer + 1][neuron]);
        }
    }

    return neurons.back();
}

void Model::change(double rate)
{
    for(int layer = 0; layer < get_layers_count(); layer++)
    {
        for(int i = 0; i < get_layer_size(layer); i++)
        {
            if(rand() % 2 == 0)
            {
                biases[layer][i] += ((double)rand()) / RAND_MAX * rate;
            } else {
                biases[layer][i] -= ((double)rand()) / RAND_MAX * rate;
            }
        }

        if(layer == get_layers_count() - 1)
        {
            continue;
        }

        for(int neuron = 0; neuron < get_layer_size(layer + 1); neuron++)
        {
            for(int current = 0; current < get_layer_size(layer); current++)
            {
                if(rand() % 2 == 0)
                {
                    weights[layer][get_layer_size(layer) * neuron + current] += ((double)rand()) / RAND_MAX * rate;
                } else {
                    weights[layer][get_layer_size(layer) * neuron + current] -= ((double)rand()) / RAND_MAX * rate;
                }
            }
        }

    }
}

int Model::get_layers_count()
{
    return neurons.size();
}

int Model::get_layer_size(int layer)
{
    return neurons[layer].size();
}

}