#include <iostream>

#include "algo.hpp"

namespace ai
{

ModelPtr train(ModelPtr mdl, std::function<double(ModelPtr)> count_score, int epoch_count, double max_rate, double rate_change, int batch_size)
{
    ModelPtr best = mdl;
    double best_score = count_score(best);

    double rate = max_rate;

    for(int epoch = 0; epoch < epoch_count; epoch++)
    {
        for(int j = 0; j < batch_size; j++)
        {
            ModelPtr current = std::make_shared<ai::Model>(best);
            current->change(rate);
            double score = count_score(current);

            if(score > best_score)
            {
                best_score = score;
                best = current;
            }
        }

        std::cout << "EPOCH " << epoch << " Best score: " << best_score << std::endl;
        // rate *= rate_change;
        // rate = (1 + cos((double)epoch * rate_change)) * max_rate / 2;
        rate = abs((double(epoch * rate_change) - double(int(epoch * rate_change / max_rate)) * double(epoch)) - max_rate / 2) * 2;
        
        // std::cout << rate << '\n';
    }

    return best;
}

double activator_normal(double x)
{
    return x;
}

double activator_sigmoid(double x)
{
    return 1.0f / ( 1.0f + exp(-x) );
}

}
