// Snake test

#include <iostream>
#include <math.h>
#include <thread>

#include <SDL2/SDL.h>

#include "Model.hpp"
#include "algo.hpp"

using ai::ModelPtr;

bool rendering = false;
SDL_Renderer *renderer;

const int tail_size = 1;

// Snake tester
double test_snake(ModelPtr model)
{
    double score = 0;
    
    for(int g = 0; g < 1; g++)
    {
        std::vector<std::pair<int, int>> tail(tail_size);
        std::pair<int, int> apple = { 5, 5 };
        tail[0].first = rand() % 10;
        tail[0].second = rand() % 10;
        // tail[0].first = 3;
        // tail[0].second = 3;
        for(int i = 1; i < tail_size; i++)
        {
            tail[i].first = tail[0].first;
            tail[i].second = tail[0].second + i;
        }

        {
            bool selected = false;
            while(!selected)
            {
                selected = true;
                apple.first = rand() % 10;
                apple.second = rand() % 10;
                for(auto elem : tail)
                {
                    if(apple == elem)
                    {
                        selected = false;
                        break;
                    }
                }
            }
        }

        for(int i = 0; i < 500; i++)
        {
            // Encoding request
            std::vector<double> in;
            for(auto e : tail)
            {
                in.push_back(e.first);
                in.push_back(e.second);
            }
            in.push_back(apple.first);
            in.push_back(apple.second);

            std::vector<double> out = model->process(in);

            // Decoding response
            double max_sure = 0;

            int vx = 0;
            int vy = 0;
            
            for(int j = 0; j < 4; j++)
            {
                if(out[j] > max_sure)
                {
                    max_sure = out[j];
                    switch(j)
                    {
                        case 0: // Left
                            vx = -1;
                            vy = 0;
                            break;
                        case 1: // Right
                            vx = 1;
                            vy = 0;
                            break;
                        case 2: // Up
                            vx = 0;
                            vy = 1;
                            break;
                        case 3: // Down
                            vx = 0;
                            vy = -1;
                            break;
                    }
                }
            }
            // std::cout << vx << ' ' << vy << std::endl;

            // Moving
            for(int k = tail_size - 1; k >= 1; k--)
            {
                tail[k] = tail[k - 1];
            }

            if(abs(tail[0].first - apple.first) > abs(tail[0].first + vx - apple.first))
            {
                score += 1;
            } else if(abs(tail[0].second - apple.second) > abs(tail[0].second + vy - apple.second))
            {
                score += 1;
            } else {
                score -= 2;
            }
            tail[0].first += vx;
            tail[0].second += vy;

            if(tail[0] == apple)
            {
                //score += 500;
                bool selected = false;
                while(!selected)
                {
                    selected = true;
                    apple.first = rand() % 10;
                    apple.second = rand() % 10;
                    for(auto elem : tail)
                    {
                        if(apple == elem)
                        {
                            selected = false;
                            break;
                        }
                    }
                }
            }

            if(rendering)
            {
                SDL_Event event;
                while(SDL_PollEvent(&event))
                {
                    if(event.type == SDL_KEYDOWN && event.key.type == SDLK_SPACE)
                    {
                        return 0;
                    }
                }

                const int delta_x = 0;
                const int delta_y = 0;
                const int rect_size = 50;
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
                SDL_SetRenderDrawColor(renderer, 200, 200, 0, 255);
                SDL_RenderDrawLine(renderer, rect_size * 11, 0, rect_size * 11, rect_size * 11);
                SDL_RenderDrawLine(renderer, 0, rect_size * 11, rect_size * 11, rect_size * 11);
                // std::cout << "===\n";
                // std::cout << tail[0].first << ' ' << tail[0].second << std::endl;
                for(int z = 0; z < tail_size; z++)
                {
                    auto e = tail[z];
                    e.first += delta_x;
                    e.second += delta_y;

                    if(z == 0)
                    {
                        SDL_SetRenderDrawColor(renderer, 200, 0, 0, 255);
                    } else {
                        SDL_SetRenderDrawColor(renderer, 0, 200, 0, 255);
                    }
                    SDL_Rect rect = { e.first * rect_size, e.second * rect_size, rect_size, rect_size };
                    SDL_RenderDrawRect(renderer, &rect);
                }
                {
                    SDL_Rect rect = { apple.first * rect_size, apple.second * rect_size, rect_size, rect_size };
                    SDL_SetRenderDrawColor(renderer, 200, 0, 200, 255);
                    SDL_RenderDrawRect(renderer, &rect);
                }
                SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
                SDL_RenderPresent(renderer);
                SDL_RenderClear(renderer);
            }

            // Damage check
            bool damage = false;
            if((tail[0].first > 10) || (tail[0].first < 0))
            {
                score -= 5000;
                break;
            } else if((tail[0].second > 10) || (tail[0].second < 0))
            {
                score -= 5000;
                break;
            } else {
                for(int k = 1; k < tail_size; k++)
                {
                    if(tail[k].first == tail[0].first && tail[k].second == tail[0].second)
                    {
                        damage = true;
                        score -= 5000;
                        break;
                    }
                }
            }

            if(damage)
            {
                break;
            }

            // score += 1;
            // score += 5 * (10 - abs(apple.first - tail[0].first));
            // score += 5 * (10 - abs(apple.second - tail[0].second));
        }
    }
    // std::cout << "Score: " << score << std::endl;

    return score;
}

int main()
{
    srand(90);

    // Model structure
    std::vector layers = { tail_size * 2 + 2, 50, 4 };
    ModelPtr original = std::make_shared<ai::Model>(layers, ai::activator_sigmoid);

    // Training model
    ModelPtr model = ai::train(original, test_snake, 5000, 10, 0.1, 100);

    SDL_Init(SDL_INIT_EVERYTHING);
    auto window = SDL_CreateWindow("0", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, 0);
    SDL_SetWindowTitle(window, "Snake Model");
    renderer = SDL_CreateRenderer(window, -1, 0);

    bool running = true;
    rendering = true;
    for(int i = 0; i < 50000; i++)
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            if(event.type == SDL_QUIT)
            {
                running = false;
            }
        }
        test_snake(model);
    }

    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
}
